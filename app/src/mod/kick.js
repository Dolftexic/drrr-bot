
exports.default = class Admin {
  constructor(bot) {
    this.bot = bot;
    this.on  = true;
  }

  find_user_by_name(name) {
    return Object.values(this.bot.users).find( user => 
      user.name === name
    );
  }

  kick(name) {
    console.log('KICK:', name);

    const user = this.find_user_by_name(name);

    if(user)
      this.bot.kick(user.id);
    else
      console.log('That guy isnt here m8.');
  }

  ban(name) {
    console.log('BAN:', name);

    const user = this.find_user_by_name(name);

    if(user)
      this.bot.banAndReport(user.id);
    else
      console.log('Yea that guy isnt here m8.');
  }

  givehost(name) {
    console.log('GIVEHOST: ', name);

    const user = this.find_user_by_name(name);

    if(user)
      this.bot.givehost(user.id);
    else 
      console.log('Yeah, that guy isnt here m8.');
  }

  giverights(args) {
    const parts = args.split(' ');
    const lvl   = parseInt(parts[0]);
    const name  = parts.slice(1).join(' ');

    console.log(`GIVERIGHTS ${lvl} to ${name}`);

    const user = this.find_user_by_name(name);
    console.log('USER', user);

    if( user && user.tripcode && !isNaN(lvl) ) {
      console.log('That user exists');
      this.bot.config.rights[user.tripcode] = lvl;
      this.bot.write_config();
    }
    else {
      console.log('Everything failed.');
    }
  }
}

