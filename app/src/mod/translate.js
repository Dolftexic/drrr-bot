
const request   = require('request');
const translate = require('@vitalets/google-translate-api');


exports.default = class Translate {
    constructor(bot) {
      this.bot = bot;
      this.on  = true;

      this.s_max  = 10;
      this.s_to   = 300; // Timeout for stupidFunction
      this.s_busy = false;
    }

    sanitize(str) {
      if( str[0] === '!' || str[0] === '/' )
        return str.substr(1);
      else
        return str;
    }
    
    async stupidFunction(n, sl, tl, queryUser) {
      console.log({n, sl, tl, queryUser});
      if(n) {
        translate(queryUser, { from: sl, to: tl }).then(res => {
          console.log(res.text);
          const diff = Math.floor( Math.random()*50 ); // Small difference to avoid being detected as spam. (Maybe)
          
          setTimeout( () =>
            this.stupidFunction(n-1, tl, sl, this.sanitize(res.text)),
            300 + diff
          );
        })
        .catch( err => {
          console.error(err);
        });
      }
      else {
        this.s_busy = false;
        this.bot.message(queryUser);
      }
    }

    s(userInput) {
      console.log('S:', userInput);
      if(!this.s_busy) {
        const parts       = userInput.split(/\s+/);
        const [n, sl, tl] = [ parts[0], parts[1], parts[2] ];
        const queryUser   = this.sanitize( parts.slice(3).join(' ') );

        if(n > this.s_max)
          this.bot.message('Sowwy too much fow me UwU');
        else {
          this.s_busy = true;
          this.stupidFunction(n, sl, tl, queryUser);
        }
      }
    }

    async translate(userInput) {
      if(userInput === 'v')
        userInput = this.bot.talks.slice(-1)[0].msg;

      console.log('TRANSLATE:', userInput);
      //https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=de&dt=t&q=hello
      const parts = userInput.split(/\s+/);
      const sl = parts[0]; 
      const tl = parts[1]; 
      let q  = parts.slice(2).join(' ').trim();
      
      if(new Set(q).size === 1 && q[0] === 'v')
        q = this.bot.talks[ this.bot.talks.length-q.length-1 ].msg;
      
      const queryUser = this.sanitize(q);

      translate(queryUser, {from: sl, to: tl}).then(res => {
        console.log(res.text);
        this.bot.message( this.sanitize(res.text) );
        /*
        console.log(res.from.text.autoCorrected);
        
        console.log(res.from.text.value);
        //=> I [speak] Dutch!
        console.log(res.from.text.didYouMean);
        //=> false
        */
      }).catch(err => {
          console.error(err);
      });
    }
  
    run(userInput) {
      this.gif(userInput);
    }
  }
  
